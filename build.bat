@echo off
gcc main.c -o SpaceInvaders.exe -O1 -Wall -std=c11 -Wno-missing-braces -I raylib/ -L raylib/ -lraylib -lopengl32 -lgdi32 -lwinmm

if %errorlevel% == 0 (
	if "%1" == "--run" (
		start SpaceInvaders.exe
	)
)

exit /b