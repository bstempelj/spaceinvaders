# Space Invaders

How to run on Windows using MinGW?
```bash
$ gcc main.c -o SpaceInvaders.exe -O1 -Wall -std=c11 -Wno-missing-braces -I raylib/ -L raylib/ -lraylib -lopengl32 -lgdi32 -lwinmm
$ ./SpaceInvaders
```
## How it looks
![](howitlooks.png)