#include "raylib.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// -------------------------------------------------------------------------------------------------
// Defines
// -------------------------------------------------------------------------------------------------
#define SPRITE_SCALE_FACTOR 3.0f
#define SPRITE_WIDTH 16.0f
#define SPRITE_HEIGHT 8.0f

#define WINDOW_WIDTH (int)(SPRITE_WIDTH * SPRITE_SCALE_FACTOR * 14)
#define WINDOW_HEIGHT (int)(SPRITE_HEIGHT * SPRITE_SCALE_FACTOR * 30)

#define PLAYER_SPEED 4.0f
#define PLAYER_BULLET_SPEED 8.0f

#define ENEMIES_PER_ROW 11
#define NUMBER_OF_ROWS 5

#define EXPLODING_ANIMATION_FRAMES 10

#define MAX_PLAYER_BULLETS 3
#define MAX_ENEMIES (ENEMIES_PER_ROW * NUMBER_OF_ROWS) // 55

// #define DEBUG

// -------------------------------------------------------------------------------------------------
// Structure Definitions
// -------------------------------------------------------------------------------------------------
typedef enum {
	TITLE = 0,
	GAMEPLAY,
	GAMEOVER
} GameScreen;

typedef enum {
	OCTOPUS = 0,
	CRAB,
	SQUID
} EnemyType;

typedef struct {
	bool fired;
	Vector2 position;
} PlayerBullet;

typedef struct {
	Vector2 position;
	Rectangle spriteClip;
} Player;

typedef struct {
	int exploding;
	bool dead;
	EnemyType type;
	Vector2 position;
	Rectangle spriteClip;
} Enemy;

// -------------------------------------------------------------------------------------------------
// Global Variables
// -------------------------------------------------------------------------------------------------
static Player player = { 0 };
static PlayerBullet playerBullets[MAX_PLAYER_BULLETS] = { 0 };
static Enemy enemies[MAX_ENEMIES] = { 0 };
static Texture2D sprites;
static GameScreen currentScreen = GAMEPLAY;

static int windowCenterX = WINDOW_WIDTH/2.0f;
static int windowCenterY = WINDOW_HEIGHT/2.0f;

static int numEnemies = 0;
static int framesCounter = 0;
static int framesSpeed = 3;
static int currentFrame = 0;

static bool pause = false;
static Rectangle boxCollision = { 0 };

// -------------------------------------------------------------------------------------------------
// Function Declarations
// -------------------------------------------------------------------------------------------------
Enemy CreateEnemy(EnemyType type, int x, int y);

void DrawGrid2D(void);
void DrawSprite(Rectangle sprite, Vector2 position);
void DrawPlayerBullets(void);
void DrawPlayer(void);
void DrawEnemies(void);

void UpdateGame(void);
void DrawGame(void);

// -------------------------------------------------------------------------------------------------
// Program Entry Point
// -------------------------------------------------------------------------------------------------
int main(void) {
	InitWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Space Invaders");
	SetTargetFPS(60);

	sprites = LoadTexture("assets/sprites.png");

	// init player
	player.position = (Vector2){
		(float)WINDOW_WIDTH/2,
		(float)(WINDOW_HEIGHT - SPRITE_HEIGHT*SPRITE_SCALE_FACTOR)
	};
	player.spriteClip = (Rectangle){ 1.0f, 49.0f, SPRITE_WIDTH, SPRITE_HEIGHT };

	// create row of octopus enemies
	// enemies[0] = CreateEnemy(
	// 	OCTOPUS,
	// 	WINDOW_WIDTH/2.0f - SPRITE_WIDTH*SPRITE_SCALE_FACTOR,
	// 	WINDOW_HEIGHT/2.0f
	// );
	// enemies[1] = CreateEnemy(
	// 	OCTOPUS,
	// 	WINDOW_WIDTH/2.0f,
	// 	WINDOW_HEIGHT/2.0f
	// );
	// enemies[2] = CreateEnemy(
	// 	OCTOPUS,
	// 	WINDOW_WIDTH/2.0f + SPRITE_WIDTH*SPRITE_SCALE_FACTOR,
	// 	WINDOW_HEIGHT/2.0f
	// );

	int windowTopOffsetRow = 7;
	int scaledSpriteWidth = SPRITE_WIDTH*SPRITE_SCALE_FACTOR;
	int scaledSpriteHeight = SPRITE_HEIGHT*SPRITE_SCALE_FACTOR;

	for (int row = 0; row < NUMBER_OF_ROWS; row++) {
		for (int i = 0; i < ENEMIES_PER_ROW; i++) {

			int startX = windowCenterX;
			startX -= (scaledSpriteWidth*ENEMIES_PER_ROW)/2.0f;
			startX += scaledSpriteWidth/2.0f;

			int enemyIndex = ENEMIES_PER_ROW*row + i;
			enemies[enemyIndex] = CreateEnemy(
				(row < 1) ? SQUID : (row < 3) ? CRAB : OCTOPUS,
				startX + scaledSpriteWidth*i,
				scaledSpriteHeight*windowTopOffsetRow + scaledSpriteHeight*row*2.0f
			);
		}
	}

	while (!WindowShouldClose()) {
		UpdateGame();
		DrawGame();
	}
	CloseWindow();

	return 0;
}

void UpdateGame(void) {
	framesCounter++;

	if (framesCounter >= (60/framesSpeed)) {
		framesCounter = 0;
		currentFrame++;

		if (currentFrame > 1) currentFrame = 0;

		// change enemy animation frame
		for (int i = 0; i < numEnemies; i++) {
			if (currentFrame == 0) {
				enemies[i].spriteClip.y = 1.0f;
			} else {
				enemies[i].spriteClip.y = 11.0f;
			}
		}
	}

	switch(currentScreen) {
		case TITLE: {
			// TODO: Update TITLE screen variables here!

			// Press enter to change to GAMEPLAY screen
			if (IsKeyPressed(KEY_ENTER)) {
				currentScreen = GAMEPLAY;
			}
		} break;
		case GAMEPLAY: {
			// TODO: Update GAMEPLAY screen variables here!
			if (IsKeyDown(KEY_RIGHT)) player.position.x += PLAYER_SPEED;
			if (IsKeyDown(KEY_LEFT))  player.position.x -= PLAYER_SPEED;

			// if (IsKeyPressed(KEY_P)) pause = !pause;

			// if (!pause) boxA.x += boxASpeedX;

			// reset ammunition if bullet goes off-screen
			for (int bi = 0; bi < MAX_PLAYER_BULLETS; bi++) {
				PlayerBullet *playerBullet = &playerBullets[bi];

				// fire bullet when player presses the space button
				if (IsKeyPressed(KEY_SPACE)) {
					if (!playerBullet->fired) {
						playerBullet->fired = true;
						playerBullet->position.x = player.position.x + 1.0f;
						playerBullet->position.y = player.position.y;
						break;
					}
				}

				// if the bullet goes offscreen
				if (playerBullet->position.y <= 0) {
					playerBullet->fired = false;
				}

				// a fired bullet should move every frame
				if (playerBullet->fired) {
					playerBullet->position.y -= PLAYER_BULLET_SPEED;

					// check if bullet hit an enemy
					for (int ei = 0; ei < numEnemies; ei++) {
						Enemy *enemy = &enemies[ei];

						if (!enemy->dead) {
							// TODO: add bulletSprite to globals
							Rectangle bulletSprite = {
								playerBullet->position.x, playerBullet->position.y,
								1.0f * SPRITE_SCALE_FACTOR, 8.0f * SPRITE_SCALE_FACTOR
							};

							Rectangle enemySprite = {
								enemy->position.x, enemy->position.y,
								SPRITE_WIDTH * SPRITE_SCALE_FACTOR, SPRITE_HEIGHT * SPRITE_SCALE_FACTOR
							};

							bool collision = CheckCollisionRecs(bulletSprite, enemySprite);
							if (collision) {
								boxCollision = GetCollisionRec(bulletSprite, enemySprite);
								enemy->exploding = EXPLODING_ANIMATION_FRAMES;
								enemy->dead = true;
								playerBullet->fired = false;
								break;
							}
						}
					}
				}
			}


			// Press enter to change to GAMEOVER screen
			if (IsKeyPressed(KEY_ENTER)) {
				currentScreen = GAMEOVER;
			}
		} break;
		case GAMEOVER: {
			// TODO: Update GAMEOVER screen variables here!

			// Press enter to return to TITLE screen
			if (IsKeyPressed(KEY_ENTER)) {
				currentScreen = TITLE;
			}
		} break;
	}
}

void DrawGame(void) {
	BeginDrawing();
		#ifdef DEBUG
			ClearBackground(RAYWHITE);
		#else
			ClearBackground(BLACK);
		#endif

		switch(currentScreen) {
			case TITLE: {
				// TODO: Draw TITLE screen here!
				DrawRectangle(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, GREEN);
				DrawText("TITLE SCREEN", 20, 20, 40, DARKGREEN);
				DrawText("PRESS ENTER or TAP to JUMP to GAMEPLAY SCREEN", 120, 220, 20, DARKGREEN);
			} break;
			case GAMEPLAY: {
				DrawEnemies();
				DrawPlayerBullets();
				DrawPlayer();

				DrawRectangle(boxCollision.x, boxCollision.y, boxCollision.width, boxCollision.height, GREEN);

				// #ifdef DEBUG
				// 	DrawGrid2D();
				// #endif

				// TODO: Draw GAMEPLAY screen here!
				// DrawRectangle(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, PURPLE);
				// DrawText("GAMEPLAY SCREEN", 20, 20, 40, MAROON);
				// DrawText("PRESS ENTER or TAP to JUMP to GAMEOVER SCREEN", 130, 220, 20, MAROON);

			} break;
			case GAMEOVER: {
				// TODO: Draw GAMEOVER screen here!
				DrawRectangle(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, BLUE);
				DrawText("GAMEOVER SCREEN", 20, 20, 40, DARKBLUE);
				DrawText("PRESS ENTER or TAP to RETURN to TITLE SCREEN", 120, 220, 20, DARKBLUE);
			} break;
		}
	EndDrawing();
}

Enemy CreateEnemy(EnemyType type, int x, int y) {
	if (numEnemies >= MAX_ENEMIES) {
		fprintf(stderr, "ERROR: Failed to create enemy. Exceeded max number of enemies.\n");
		exit(1);
	}

	Enemy enemy = (Enemy){
		.exploding = 0,
		.dead = false,
		.type = type,
		.position = (Vector2){ (float)x, (float)y }
	};

	switch (enemy.type) {
		case SQUID:
			enemy.spriteClip = (Rectangle){ 1.0f, 1.0f, SPRITE_WIDTH, SPRITE_HEIGHT };
			break;
		case CRAB:
			enemy.spriteClip = (Rectangle){ 19.0f, 1.0f, SPRITE_WIDTH, SPRITE_HEIGHT };
			break;
		case OCTOPUS:
			enemy.spriteClip = (Rectangle){ 37.0f, 1.0f, SPRITE_WIDTH, SPRITE_HEIGHT };
			break;
	}

	numEnemies++;

	return enemy;
}

void DrawPlayerBullets(void) {
	Rectangle bulletSprite = { 55.0f, 49.0f, 1.0f, 8.0f };

	for (int i = 0; i < MAX_PLAYER_BULLETS; i++) {
		PlayerBullet *playerBullet = &playerBullets[i];

		if (playerBullet->fired) {
			playerBullet->position.y -= PLAYER_BULLET_SPEED;
			DrawSprite(bulletSprite, playerBullet->position);
		}
	}
}

void DrawPlayer(void) {
	Rectangle playerSprite = { 1.0f, 49.0f, SPRITE_WIDTH, SPRITE_HEIGHT };
	DrawSprite(playerSprite, player.position);
}

void DrawEnemies(void) {
	for (int i = 0; i < numEnemies; i++) {
		Enemy *enemy = &enemies[i];

		if (enemy->exploding > 0) {
			Rectangle explosionSprite = { 55.0f, 1.0f, SPRITE_WIDTH, SPRITE_HEIGHT };
			DrawSprite(explosionSprite, enemy->position);
			enemy->exploding--;
		}

		if (!enemy->dead) {
			DrawSprite(enemy->spriteClip, enemy->position);
		}
	}
}

void DrawSprite(Rectangle spriteClip, Vector2 position) {
	if (spriteClip.width == 0 || spriteClip.height == 0) {
		fprintf(stderr, "Sprite width or height is not defined!\n");
		exit(1);
	}

	float scaledSpriteWidth = spriteClip.width * SPRITE_SCALE_FACTOR;
	float scaledSpriteHeight = spriteClip.height * SPRITE_SCALE_FACTOR;

	Rectangle dest = { position.x, position.y, scaledSpriteWidth, scaledSpriteHeight };
	Vector2 origin = { scaledSpriteWidth/2.0f, scaledSpriteHeight/2.0f };
	DrawTexturePro(sprites, spriteClip, dest, origin, 0.0f, WHITE);

	#ifdef DEBUG
		int rectX = position.x - scaledSpriteWidth/2.0f;
		int rectY = position.y - scaledSpriteHeight/2.0f;
		int rectWidth = scaledSpriteWidth;
		int rectHeight = scaledSpriteHeight;

		DrawRectangleLines(rectX, rectY, rectWidth, rectHeight, RED);
	#endif
}

void DrawGrid2D(void) {
	int scaledWidth = (int)(SPRITE_WIDTH * SPRITE_SCALE_FACTOR);
	int scaledHeight = (int)(SPRITE_HEIGHT * SPRITE_SCALE_FACTOR);

	for (int y = 0; y < WINDOW_HEIGHT; y += scaledHeight) {
		for (int x = 0; x < WINDOW_WIDTH; x += scaledWidth) {
			DrawRectangleLines(x, y, scaledWidth, scaledHeight, RED);
		}
	}
}